#!/bin/bash
#
#############################################################################
#                                                                           #
#        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE                        #
#                    Version 2, December 2004                               #
#                                                                           #
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>                          #
#                                                                           #
# Everyone is permitted to copy and distribute verbatim or modified         #
# copies of this license document, and changing it is allowed as long       #
# as the name is changed.                                                   #
#                                                                           #
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE                    #
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION         #
#                                                                           #
#  0. You just DO WHAT THE FUCK YOU WANT TO.                                #
#                                                                           #
#############################################################################
#
#  scripted by Tim Foerster <timhormersdorf@googlemail.com>    
#                           >>> Copyright 2013 <<<	
#

DISK1="empty"
DISK2="empty"
MOUNT_DIR="/mnt/"
BACKUP_DIR="backup/"

#check if executed by root
if [ $EUID -ne 0 ]; then
   echo "This script must be run as root" 1>&2
   exit 1
else
	if [ "$#" -eq "1" ]; then
		#show SerialNo of available disks
		if [ "$1" = "--getDisks" ]; then
			for i in $(ls /dev/ | grep -Eo '^(h|s)d[a-z]$'); do
				echo "$(sudo hdparm -i /dev/$i | grep SerialNo | awk '{split($NF,ary,"="); print ary[2]}') -> /dev/$i"
			done
		fi
	#locking for disks 
	else
		for i in $(ls /dev/ | grep -Eo '^(h|s)d[a-z]$'); do
            if [ $1 =  $(sudo hdparm -i /dev/$i | grep SerialNo | awk '{split($NF,ary,"="); print ary[2]}') ]; then
					DISK1="/dev/$i"
					echo -e "DISK1: $DISK1 \e[00;32m[ OK ]\e[00m \n"
				elif [ $2 =  $(sudo hdparm -i /dev/$i | grep SerialNo | awk '{split($NF,ary,"="); print ary[2]}') ]; then
					DISK2="/dev/$i"
					echo -e "DISK2: $DISK2 \e[00;32m[ OK ]\e[00m \n"
				fi
      done
		if [ "$DISK1" = "empty" ]; then
			echo "No disk found!"
			exit 1
		fi
		if [ "$DISK2" = "empty" ]; then
         echo "Second disk not set or found ..."
			if [ -n "$2" ]; then
				umount $DISK1
				mount $DISK1 $MOUNT_DIR
				if [ ! -d "$MOUNT_DIR$BACKUP_DIR" ]; then
					mkdir -p $MOUNT_DIR$BACKUP_DIR
				fi
				rsync -P $2 $MOUNT_DIR$BACKUP_DIR
			else
				echo "You forgot to enter a src."
				exit 1
			fi
      fi
		if [ "$DISK2" != "empty" ]; then
         if [ -n "$3" ]; then
            umount $DISK1
            mount $DISK1 $MOUNT_DIR
            if [ ! -d "$MOUNT_DIR$BACKUP_DIR" ]; then
               mkdir -p $MOUNT_DIR$BACKUP_DIR
            fi
            rsync -P $3 $MOUNT_DIR$BACKUP_DIR
				umount $DISK2
            mount $DISK2 $MOUNT_DIR
            if [ ! -d "$MOUNT_DIR$BACKUP_DIR" ]; then
               mkdir -p $MOUNT_DIR$BACKUP_DIR
            fi
            rsync -P $3 $MOUNT_DIR$BACKUP_DIR
         else
            echo "You forgot to enter a src."
				exit 1
         fi
      fi


	fi
fi

