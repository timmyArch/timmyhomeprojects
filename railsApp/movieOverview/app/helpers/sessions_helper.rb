module SessionsHelper

	#	
	#	@param user_hash from request
	#	@param user_id from session
	#	
	#	@return bool
	#
	def me? ( user_hash , id )
		user1 = User.find_by_hash(user_hash).map(&:id).join
		user2 = User.find_hash_by_id(id).map(&:id).join
		return true if user1 == user2
		return true unless params[:id]
		return false unless user1 == user2
	end

end
