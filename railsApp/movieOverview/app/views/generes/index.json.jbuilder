json.array!(@generes) do |genere|
  json.extract! genere, :title
  json.url genere_url(genere, format: :json)
end
