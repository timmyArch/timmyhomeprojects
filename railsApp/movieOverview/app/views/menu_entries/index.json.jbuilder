json.array!(@menu_entries) do |menu_entry|
  json.extract! menu_entry, :title
  json.url menu_entry_url(menu_entry, format: :json)
end
