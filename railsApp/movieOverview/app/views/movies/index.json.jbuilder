json.array!(@movies) do |movie|
  json.extract! movie, :name, :description, :rating, :genere
  json.url movie_url(movie, format: :json)
end
