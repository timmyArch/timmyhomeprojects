class SessionsController < ApplicationController
 
        before_filter :authenticate_user, :except => [:login, :login_attempt, :logout]
        before_filter :save_login_state, :only => [:index, :login, :login_attempt]

        def home
        end

        def profile
					@user = User.find_by_hash(params[:id]) unless params[:id].nil?
					@menu = MenuEntrie.all	


					unless params[:menu].nil?
						@menu_active = params[:menu]
					else
						@menu_active = MenuEntrie.find_by_title("Home").title
        	end
				end

        def setting
					if request.post?
						case 
							when params[:key] == "pagination"
								@user = UserSettings.find_by_user_id(session[:user_id])
								@user.pagination = true if params[:checkbox] == "on"
								@user.pagination = false unless params[:checkbox]
								(@user.save) ? flash[:success] = "Wert wurde angepasst!" : flash[:error] = "Es ist ein Fehler aufgetreten" 
						end						
					end
        end

        def login
        end

        def login_attempt
                authorized_user = User.authenticate(params[:username_or_email],params[:login_password])
                if authorized_user
                        session[:user_id] = authorized_user.id
                        flash[:success] = "Wow Welcome again, you logged in as #{authorized_user.username}"
                        redirect_to(:action => 'home')


                else
                        flash[:warning] = "Invalid Username or Password"
                        render "login"        
                end
        end

        def logout
                session[:user_id] = nil
								flash[:success] = "Erfolgreich ausgeloggt"
                redirect_to :action => 'login'
        end

end
