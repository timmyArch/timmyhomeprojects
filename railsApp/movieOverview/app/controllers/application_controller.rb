class ApplicationController < ActionController::Base
	

  protect_from_forgery
  protected

  def authenticate_user
          unless session[:user_id]
                  redirect_to(:controller => 'sessions', :action => 'login')
                  return false
          else
    			  @current_user = User.find session[:user_id] 
						#get int array from database
						perms = Permission.where("permissions.group = ?",
													Group.find_by_user_id(session[:user_id]).id)
													.map(&:permission).uniq rescue false
						#if perms is set, then map the permission names to int values
						if perms
							perms = PERMS_ALL.map{|x| x if perms.any? {|y| x.include? y} }.compact rescue false
						end
						#if there are any errors, give him fallback role
						perms ||= PERMS_USER 
            session[:perms] = perms 
						return true
          end
  end

  #This method for prevent user to access Signup & Login Page without logout
  def save_login_state
    if session[:user_id]
            redirect_to(:controller => 'sessions', :action => 'home')
      return false
    else
      return true
    end
  end
end
