class SearchController < ApplicationController

	before_filter :authenticate_user
	
	def user
		if request.get?
			@results = User.search_by_username(params[:val]) unless params[:val].empty? unless params[:val].nil?
			result=""
			unless @results.nil?
				@results.each do |x| 
					result+="<li role='presentation'><a role='menuitem' tabindex='-1' href=\"/profile/#{User.find_hash_by_id(x.id).map(&:uid).join}\">#{x.username}</a></li>"
				end
				if @results.empty?
					result="<li role='presentation'><a role='menuitem' tabindex='-1' href=\"#\">Keine Ergebnisse gefunden</a></li>"
				end
			end
			render text: result
		end
	end
  
	def post
    if request.get?
  	   @results = Post.search_by_content(params[:val]) unless params[:val].empty? unless params[:val].nil?
       result=""
       unless @results.nil?
       		@results.each do |x| 
       			result+="<li role='presentation'><a role='menuitem' tabindex='-1' href=\"/profile/#{User.find_hash_by_id(x.user_id).map(&:uid).join}\">#{x.username}</a></li>"
       		end
       		if @results.empty?
       	 		result="<li role='presentation'><a role='menuitem' tabindex='-1' href=\"#\">Keine Ergebnisse gefunden</a></li>"
       		end
      	end
   			render text: result
  	end
	end

	def index
		if request.post?
			searchString = params[:search]
			unless searchString.empty?
				@post = Post.search_by_content(searchString)
				@user = User.search_by_username(searchString)
				@movieD = Movie.search_by_description(searchString)
				@movieN = Movie.search_by_name(searchString)
				@results = []
				buff = {}; @user.map{|x| buff = buff.merge({x.id => { :from => "User", :name => x.username, :uid => x.uid}})}
				@results << buff
				buff = {}; @post.map{|x| buff = buff.merge({x.id => { :from => "Post", :name => x.username, :uid => x.uid}})}
				@results << buff
				buff = {}; @movieD.map{|x| buff = buff.merge({x.id => { :from => "Filme - Beschreibung", :name => "#{x.name} (Rating: #{x.rating})", :id => x.id}})}
				@results << buff
				buff = {}; @movieN.map{|x| buff = buff.merge({x.id => { :from => "Filme - Name", :name => "#{x.name} (Rating: #{x.rating})", :id => x.id}})}
				@results << buff				
			end 
		end
		render "results"
	end

end
