class UsersController < ApplicationController  

	before_filter :authenticate_user, :only => :change_username 

	def getCurrentUser
		return User.find session[:user_id]
	end

	def create
		if request.post?
    	@user = User.new(params[:user])
    	if @user.save
#				@group = Group.new()
#				@group.user_id = @user.id
#				@group.name = @user.username
#				@group.description = @user.username + "-group autogen"
#				if @group.save
    	  	flash[:success] = "You signed up successfully"
					session[:user_id] = User.find_by_username(params[:user][:username]).id
					@current_user = getCurrentUser
					render "sessions/home"
#				else 
#					flash[:error] = "Verarbeitungsfehler -> Bitte kontaktieren Sie den Inhaber"
#					render "new" 
#				end
    	else
    	  flash[:error] = "Form is invalid"
				render "new"
    	end
		else
			render "new"
		end
  end
	
	def change_username
		if request.post?
			@user = User.find session[:user_id]
			@user.username = params[:username]
			if @user.save
				flash[:info] = "Wow, du hast deinen Username geändert"
				@current_user = getCurrentUser
				render "sessions/profile"
			else
				flash[:error] = "Bitte prüfe deinen Username. Vielleicht gibts den schon? Ich hab doch keine Ahnung. Sry -.-"
				@current_user = getCurrentUser
				render "sessions/profile"
			end		
		end
	end	
end
