class MoviesController < ApplicationController

	before_filter :authenticate_user, :only => :new

  def new
		if request.post?
			genere = Genere.find_by_title(params[:genere]).id
			unless genere.nil?
				moviename = params[:moviename]
				@movie = Movie.new
				@movie.name = moviename
				# @movie.rating will be set automatically via trigger
				@movie.genere = genere
				@movie.description = moviename
				if @movie.save
					flash[:success] = "Film wurde erstellt."
					redirect_to :action => :show, :movie_id => moviename
				else
					begin
						unless Movie.find_by_name(moviename).nil?
          		flash[:success] = "Film wurde gefunden."
          		redirect_to :action => :show , :movie_id => moviename
						end	
					rescue => e 
							flash[:error] = "Film konnte nicht angelegt werden, bitte Filmname prüfen"+e.message
					end
				end
			else
				flash[:error] = "Genere konnte nicht gefunden werden"
			end
		end
  end

  def show                                                                                                   
    if movie=params[:movie_id]
      @movie = Movie.find_by_name(movie) unless movie.is_num?
			@movie = Movie.find(movie) if movie.is_num?
			begin
				@media = @movie.media_resources
				if @media.empty?
					trailer = Movie.getTrailerURL(@movie.name)
					images = Movie.getAryImages(@movie.name)
					flash[:info] = trailer
					if !trailer.nil? || !images.nil?
						add = MediaResource.new
						add.typ_id = GA_VIDEO
						add.src = trailer
						add.movie_id = @movie.id
						(add.save) ? (debug = "Trailer hinzugefügt") : (debug = "Fehler beim hinzufügen des Trailers")
						(images.length).times do |x| 
							add = MediaResource.new
							add.typ_id = GA_IMAGE
							add.src = images[x]
							add.movie_id = @movie.id
							(add.save) ? (debug += "<br>Bild ##{x} hinzugefügt") : (debug += "<br>Fehler beim hinzufügen des Bildes ##{x}")
						end
					end
				end
			rescue => e
        flash[:error] = "Es ist ein Fehler aufgetreten "+e.message
        redirect_to :home
      end 
		end                                                                                                                                       
  end
 

	def rate(data = [])
		if request.post? || !data
			user_id = session[:user_id]
			if data
				movie_id = params[:movie_id] 
				rating = params[:star]
			else
				movie_id = data[0]
				rating = data[1]
			end
			out="#{movie_id} - #{rating}" 
			if Rating.isDuplicate?(user_id, movie_id)
				id = Rating.find_id_by_user_and_movie(user_id,movie_id)
				@record = Rating.find(id)
				@record.value = rating
				(@record.save) ? (flash[:success] = "Rating wurde erfolgreich angpasst") : (flash[:error] = "Bearbeitungsfehler beim Rating anpassen"+out)
				redirect_to :back				
			else
				@record = Rating.new
				@record.movie_id = movie_id
				@record.user_id = user_id
				@record.value = rating
				(@record.save) ? (flash[:success] = "Rating wurde erfolgreich gesetzt") : (flash[:error] = "Bearbeitungsfehler beim Rating erstellen"+out)
				redirect_to :back
			end
		end
	end

	private

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.require(:movie).permit(:name, :description, :rating, :genere)
    end
end

class String
  def is_num?
    true if Float(self) rescue false
  end
end
