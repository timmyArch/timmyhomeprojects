class Rating < ActiveRecord::Base
	validates :value, :inclusion => 1..5
	validates :movie_id, :length => { :in => 1..255 }
	validates :user_id, :length => { :in => 1..255 }
	belongs_to :user

	### scopes
		
		scope :find_id_by_user_and_movie, -> (user_id,movie_id) {
			select("id")
			.where("user_id = ? AND movie_id = ?", user_id, movie_id)
		}

	##

	def self.isDuplicate?(user_id,movie_id)
		@db = Rating.find_id_by_user_and_movie(user_id,movie_id)
		(@db.nil? || @db.empty?) ? false : true
	end

end
