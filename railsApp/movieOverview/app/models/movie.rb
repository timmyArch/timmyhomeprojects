class Movie < ActiveRecord::Base

	NAME_REGEX = /\A[a-zA-Z0-9]{1,}(_[a-zA-Z0-9]{1,})?\z/                                                                                                                  
  validates :name, :presence => true, :uniqueness => true, :format => NAME_REGEX

	has_many :media_resources

	### scopes
	
		scope :search_by_name, -> (name) { select("id,name,rating").where("lower(name) LIKE lower(?)", "%"+name+"%") }
		scope :search_by_description, -> (description) { select("id,name,rating").where("lower(description) LIKE lower(?)", "%"+description+"%") }

	## 

	def self.getTrailerURL(query = nil)
		unless query.nil?
			begin
				query = query.split(/(?=[A-Z])/).join(' ')
				@apiData = AddGoogleApiAdminTable.last
				GoogleAjax.referrer = @apiData.referrer
				GoogleAjax.api_key = @apiData.api_key
				@api = GoogleAjax::Search.video(query.gsub("_", " ")+" Trailer")[:results].map{|x| x[:url]}
				@api.first
			rescue => e
				return e
			end
		else
			return nil
		end
	end

	def self.getAryImages(query = nil)
		unless query.nil?
			begin
				query = query.split(/(?=[A-Z])/).join(' ')
				@apiData = AddGoogleApiAdminTable.last
				GoogleAjax.referrer = @apiData.referrer
				GoogleAjax.api_key = @apiData.api_key
				@api = GoogleAjax::Search.images(query.gsub("_", " "))[:results].map{|x| x[:tb_url]}
			rescue => e
				return e
			end
		else
			return nil
		end
	end

end

