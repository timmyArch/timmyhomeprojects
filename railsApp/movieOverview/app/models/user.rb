class User < ActiveRecord::Base

  attr_accessor :password

	has_many :groups
	has_many :posts
	has_many :user_settings , :class_name => "UserSettings"	
	has_many :ratings

  before_save :encrypt_password
  after_save :clear_password
	after_save :generate_usersettings

  EMAIL_REGEX = /\A^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$\Z/i
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
  validates :email, :presence => true, :uniqueness => true, :format => EMAIL_REGEX
  validates :password, :confirmation => true
  #Only on Create so other actions like update password attribute can be nil
  validates_length_of :password, :in => 6..20, :on => :create

  attr_accessible :username, :email, :password, :password_confirmation


	#define here scopes ...
		
		scope :find_by_hash , -> (hash) { where("encode(digest(CONCAT(id, salt) , #{HASH} ),'hex') = ?", hash) }
		scope :find_hash_by_id , -> (id) { select("id, encode(digest(CONCAT(id,salt),#{HASH}), 'hex' ) AS uid, username").where("id = ?", id) }
		scope :find_hash_by_username , -> (username) { select("id, encode(digest(CONCAT(id,salt), #{HASH}),'hex') AS uid, username").where("username = ?", username) } 
		scope :search_by_username , -> (val) { select("id , encode(digest(concat(id,salt), #{HASH}),'hex') as uid, username").where("lower(username) LIKE lower(?)", "%"+val+"%")}
				

	#end


  def self.authenticate(username_or_email="", login_password="")

    if  EMAIL_REGEX.match(username_or_email)    
      user = User.find_by_email(username_or_email)
    else
      user = User.find_by_username(username_or_email)
    end

    if user && user.match_password(login_password)
      return user
    else
      return false
    end
  end   

  def match_password(login_password="")
    encrypted_password == BCrypt::Engine.hash_secret(login_password, salt)
  end



  def encrypt_password
    unless password.blank?
      self.salt = BCrypt::Engine.generate_salt
      self.encrypted_password = BCrypt::Engine.hash_secret(password, salt)
    end
  end

  def clear_password
    self.password = nil
  end

	def generate_usersettings
		@user = UserSettings.new
		@user.user_id = self.id
		@user.save
	end
		

end
