class Post < ActiveRecord::Base
  belongs_to :user


	##scopes
		
		scope :search_by_content, -> (val) {
			select("DISTINCT posts.user_id as id, encode(digest(concat(posts.user_id,users.salt), #{HASH}),'hex') as uid, users.username")
			.where("lower(posts.content) LIKE lower(?)", "%"+val+"%")
			.joins("LEFT OUTER JOIN users ON posts.user_id = users.id ")
		}
		
		scope :find_posts_by_uid, -> (uid, offset) {
			select("posts.id, content, age(posts.updated_at)")
			.joins("LEFT OUTER JOIN users ON posts.user_id = users.id")
			.where("encode(digest(concat(users.id,users.salt),#{HASH}),'hex') = ?",uid)
			.limit(20)
			.offset(offset.to_i)
			.order(updated_at: :desc)
		}

	##end
end
