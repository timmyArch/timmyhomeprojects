MovieOverview::Application.routes.draw do

	#only admin actions
  resources :menu_entries
  resources :groups
	resources :posts
	resources :generes
	
  get "/search", to: 'search#index' 
	get '' , to: 'sessions#home' 
	get '/logout', to: 'sessions#logout' 	
	get '/home', to: 'sessions#home' 
	get '/profile', to: 'sessions#profile' 
	get '/profile/:id', to: 'sessions#profile'
	get '/profile/:id/:menu', to: 'sessions#profile'
	get '/profile/:menu', to: 'sessions#profile'
	post '/profile/add_state', to: 'posts#create'
	get '/posts/get_next_posts/:uid/:offset', to: 'posts#getNextPosts'
	get '/m/:movie_id' , to: 'movies#show'
	post '/movies/new'

	match '/search', to: 'search#index', via: [:get, :post]
	match '/create', to: 'users#create', via: [:get, :post]
	match '/login', to: 'session#login', via: [:get, :post]
	
	#default route
	match ':controller/:action' , via: [:get, :post]
end
