#DEFINE GLOBALS HERE

#permission constants

ADD_MOVIE 		= [ 1 , 'Film hinzufuegen' ]
DELETE_MOVIE 	= [ 2 , 'Film loeschen' ]

UPDATE_POSTS	= [ 4 , 'Posts aendern' ]
ADD_POSTS 		= [ 5 , 'Posts hinzufuegen' ]
VIEW_POSTS 		= [ 6 , 'Posts anschauen' ]
DELETE_POSTS 	= [ 7 , 'Posts löschen' ]

VIEW_HOME 		= [ 8 , 'Home -> sichtbar' ]
VIEW_INFO 		= [ 9 , 'Info -> sichtbar' ]
VIEW_MOVIES 	= [ 10 , 'Filme -> sichtbar' ]
VIEW_NOTES		= [ 11 , 'Notizen -> sichtbar' ]
VIEW_NAME 		= [ 12 , 'Name -> sichtbar' ]

SET_RATING 		= [ 13 , 'Bewerten' ]

MOVIE_UPDATE_NAME 				= [ 100 , 'Filmnamen aendern' ] 
MOVIE_UPDATE_DESCRIPTION 	= [ 101 , 'Filmbeschreibung aendern' ] 
MOVIE_REVOKE_RATINGS 			= [ 102 , 'Bewertungen aufheben' ] 
MOVIE_UPDATE_GENERE 			= [ 103 , 'Filmgenere anpassen' ] 
MOVIE_HIDE_COMMENTS				= [ 104 , 'Verberge Kommentare' ] 
MOVIE_DELETE_COMMENTS			= [ 105 , 'Loesche Kommentare' ] 
MOVIE_SET_PATH						= [ 106 , 'Setze einen Ort wo der Film zufinden ist' ] 
MOVIE_SET_OWNER						= [ 107 , 'Setze einen Besitzer wem der Film gehört' ] 

#roles
PERMS_USER 					= [VIEW_HOME,VIEW_INFO,VIEW_MOVIES,VIEW_NOTES,VIEW_NAME,SET_RATING,ADD_MOVIE,ADD_POSTS,VIEW_POSTS,DELETE_POSTS,UPDATE_POSTS]
PERMS_MODERATOR 		= [MOVIE_UPDATE_NAME,MOVIE_UPDATE_DESCRIPTION,MOVIE_UPDATE_GENERE,MOVIE_HIDE_COMMENTS]
PERMS_MOVIE_ADMIN 	= [DELETE_MOVIE,MOVIE_REVOKE_RATINGS,MOVIE_DELETE_COMMENTS,MOVIE_SET_PATH,MOVIE_SET_OWNER].concat PERMS_MODERATOR
PERMS_ALL 					= PERMS_USER.concat PERMS_MOVIE_ADMIN 

#googleapi constants
GA_VIDEO = 1
GA_IMAGE = 2

#hashing for uid
HASH = "'sha1'"
