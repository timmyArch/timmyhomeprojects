class CreateGeneres < ActiveRecord::Migration
  def change
    create_table :generes do |t|
      t.string :title

      t.timestamps
    end
  end
end
