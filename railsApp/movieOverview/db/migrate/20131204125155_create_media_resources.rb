class CreateMediaResources < ActiveRecord::Migration
  def change
    create_table :media_resources do |t|
      t.integer :type
      t.belongs_to :movie, index: true
      t.string :src

      t.timestamps
    end
  end
end
