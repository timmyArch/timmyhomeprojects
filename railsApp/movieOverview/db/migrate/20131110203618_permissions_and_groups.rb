class PermissionsAndGroups < ActiveRecord::Migration
  def change
		create_table :permissionsandgroups do |t|
        t.integer :permission
        t.integer :group
        t.timestamps
   	end
  end
end
