class SetDefaultAtUserSettingsOption < ActiveRecord::Migration
  def change
		change_column :user_settings, :pagination, :boolean, :default => true
  end
end
