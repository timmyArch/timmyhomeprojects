class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.integer :permission
      t.integer :group

      t.timestamps
    end
  end
end
