class AddTriggerForRatingDeleteOnMovieDelete < ActiveRecord::Migration
  def change
		execute %q{
			CREATE OR REPLACE FUNCTION deleteRatings() RETURNS trigger AS $$
    		BEGIN
	    		DELETE FROM ratings WHERE movie_id = OLD.id;
        	RETURN NULL; 
    		END;
			$$ LANGUAGE plpgsql;
		}
		execute %q{
			CREATE TRIGGER tidyUpRatings
				AFTER DELETE ON movies
    			FOR EACH ROW EXECUTE PROCEDURE deleteRatings();
		}
  end
end
