class CreateAddGoogleApiAdminTables < ActiveRecord::Migration
  def change
    create_table :add_google_api_admin_tables do |t|
      t.string :api_key
      t.string :referrer

      t.timestamps
    end
  end
end
