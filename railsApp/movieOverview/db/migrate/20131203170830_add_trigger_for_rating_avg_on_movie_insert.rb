class AddTriggerForRatingAvgOnMovieInsert < ActiveRecord::Migration
  def change
	
		execute %q{
			CREATE OR REPLACE FUNCTION setAvgRating() RETURNS trigger AS $$
    		BEGIN
	    		UPDATE movies SET rating = (SELECT AVG(value) FROM ratings WHERE ratings.movie_id = NEW.movie_id ) WHERE id = NEW.movie_id;
        	RETURN NULL; 
    		END;
			$$ LANGUAGE plpgsql;
		}
		execute %q{
			CREATE TRIGGER setMovieRating
				AFTER INSERT OR UPDATE OR DELETE ON ratings
    			FOR EACH ROW EXECUTE PROCEDURE setAvgRating();
		}

  end
end
