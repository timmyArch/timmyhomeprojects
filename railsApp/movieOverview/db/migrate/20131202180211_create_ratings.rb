class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.belongs_to :movie, index: true
      t.integer :value

      t.timestamps
    end
  end
end
