class AddPermission < ActiveRecord::Migration
  def change
	   create_table :permission do |t|
         t.string :name
         t.string :description
         t.timestamps
      end
  end
end
