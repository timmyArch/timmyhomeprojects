class AddGroupTrigger < ActiveRecord::Migration
  def change

		execute %q{
    	CREATE OR REPLACE FUNCTION setGroup() RETURNS TRIGGER AS $$
				BEGIN
					INSERT INTO groups (name, description, user_id,updated_at,created_at) VALUES (NEW.username,NEW.username,NEW.id,now(),now()); 
					RETURN NULL;
				END;
			$$ LANGUAGE plpgsql;
		}
    execute %q{                                                                                                                                                                                  
    	CREATE TRIGGER autoGroup 
				AFTER INSERT ON users
		    FOR EACH ROW EXECUTE PROCEDURE setGroup();
		}

  end
end
