class CreateMenuEntries < ActiveRecord::Migration
  def change
    create_table :menu_entries do |t|
      t.string :title

      t.timestamps
    end
  end
end
