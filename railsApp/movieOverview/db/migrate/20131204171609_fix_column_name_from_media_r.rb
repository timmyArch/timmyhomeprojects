class FixColumnNameFromMediaR < ActiveRecord::Migration
  def change
		rename_column :media_resources, :type, :typ_id
  end
end
