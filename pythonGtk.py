#!/usr/bin/python

# Timmy's Python Script 
# there is no magic!
#
#
#
from optparse import OptionParser
from collections import deque
import ConfigParser
import subprocess
import gtk
import os
import random
import string
import time


class EntryDialog(gtk.MessageDialog):
    def __init__(self, *args, **kwargs):
        default_value = ''
        super(EntryDialog, self).__init__(*args, **kwargs)
        entry = gtk.Entry()
        entry.set_text(str(default_value))
        entry.connect("activate",
                      lambda ent, dlg, resp: dlg.response(resp),
                      self, gtk.RESPONSE_OK)
        self.vbox.pack_end(entry, True, True, 0)
        self.vbox.show_all()
        self.entry = entry

    def set_value(self, text):
        self.entry.set_text(text)

    def run(self):
        result = super(EntryDialog, self).run()
        if result == gtk.RESPONSE_OK:
            text = self.entry.get_text()
        else:
            text = None
        return text


class Main():
    homeDir = os.getenv("HOME") + "/.timmyPython.conf"

    @staticmethod
    def mtr(option, opt_str, value, parser):
        ip = "127.0.0.1"
        aryMtr = deque([])

        inputBox = EntryDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_OK,
                               message_format="Verrate John deine IP. Er checkt das dann.")
        ip = inputBox.run()

        p = subprocess.Popen('mtr ' + ip + ' -rnc1', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        for line in p.stdout.readlines():
            aryMtr.append(line)

        if ip in aryMtr[(len(aryMtr) - 1)]:
            aryMtr.popleft()
            print "----====snip====----\n " + ", ".join(aryMtr).replace(',', '') + "----====snap====----"
        else:
            message = gtk.MessageDialog(type=gtk.MESSAGE_INFO,
                                        buttons=gtk.BUTTONS_OK,
                                        message_format="John musste leider feststellen das deine IP nicht erreichbar ist. \nCheck das lieber selbst noch einmal!"
            )
            message.run()
            message.destroy()
            return 0

    @staticmethod
    def setup(option, opt_str, value, parser):
        aryConf = deque(['firstname', 'name'])
        aryConfDes = deque(['Vorname', 'Nachname'])
        config = ConfigParser.RawConfigParser()
        try:
            with open(Main.homeDir):
                message = gtk.MessageDialog(type=gtk.MESSAGE_INFO,
                                            buttons=gtk.BUTTONS_YES_NO,
                                            message_format="Willschte dat Konfigfeil wirklisch entferne?"
                )
                retVal = message.run()
                message.destroy()
                # -9 == YES | -8 == NO
                if retVal == -9:
                    return 0
                else:
                    os.remove(Main.homeDir)
                    Main.setup(True, True, True, True)

        except IOError:
            # ask for config parameters if config did not found at default path
            for i in range(0, len(aryConf)):
                inputBox = EntryDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_OK,
                                       message_format="Bitte " + aryConfDes[i] + " eingeben: ")
                aryConf[i] = inputBox.run()
            config.add_section("personal")
            config.set("personal", "firstname", aryConf[0])
            config.set("personal", "name", aryConf[1])
            with open(Main.homeDir, 'w+') as configfile:
                config.write(configfile)

    @staticmethod
    def token(option, opt_str, value, parser):
        try:
            with open(Main.homeDir):
                str = time.strftime("%d.%m.%Y %H:%M ")
                config = ConfigParser.RawConfigParser()
                config.read(Main.homeDir)
                str += config.get("personal", "firstname").lower()
                str += "." + config.get("personal", "name").lower() + " \n"
                print str
        except IOError:
            message = gtk.MessageDialog(type=gtk.MESSAGE_INFO,
                                        buttons=gtk.BUTTONS_YES_NO,
                                        message_format="John konnte kein ConfigFile finden. \n"
                                                       "Willst du eins erstellen")
            retVal = message.run()
            message.destroy()
            # -9 == YES | -8 == NO
            if retVal == -9:
                return 0
            else:
                Main.setup(True, True, True, True)
                Main.token(True, True, True, True)

    @staticmethod
    def password(option, opt_str, value, parser):
        char = string.ascii_uppercase+string.ascii_lowercase+string.digits
        rand = ''.join(random.choice(char) for i in range(18))
        print "----====snip====----\n pass: "+rand+"\n----====snap====----"

parser = OptionParser()
parser.add_option("-m", "--mtr", help="Exec the mtr check", action="callback", callback=Main.mtr)
parser.add_option("-s", "--setup", help="Run setup", action="callback", callback=Main.setup)
parser.add_option("-t", "--token", help="Print token", action="callback", callback=Main.token)
parser.add_option("-p", "--password", help="Print random password", action="callback", callback=Main.password)
(opts, args) = parser.parse_args()

