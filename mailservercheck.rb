require 'mail'
require 'logger'
require 'pstore'

module MailserverChecker

  LOGGER ||= Logger.new(STDOUT)

  class MissingParameterError < ArgumentError; end
  class InvalidParameterFormatError < TypeError; end
  class InvalidParameterError < TypeError; end

  class Processor

    def initialize configfile
      @conf = ConfigParser.parse configfile 
      Queue.set_connection filename: @conf.queue
      MailserverChecker.instance_eval {remove_const(:LOGGER)}
      MailserverChecker.const_set :LOGGER, Logger.new(@conf.logfile)
      get_senders.each do |sender|
        Queue << sender
      end
      (1..10).each do 
        check_queue
        break if Queue.empty?
        sleep 3
      end
    end

    def get_senders
      result = Array.new
      @conf.senders.each do |sender|
        @conf.recievers.each do |reciever|
          result << Sender.new(sender.to_h, { from: sender.mail, 
                                              to: reciever.mail })
          @conf.custom_senders.each do |cs| 
            result << Sender.new(cs.to_h, {from: cs.mail, to: sender.mail})
          end
        end
      end
      result
    end

    def check_queue
      queuebuffer = Array.new
      Queue.all.each do |entry|
        @conf.recievers.select{ |v| v.mail == entry.last }.each do |e|
          rec=Reciever.new(e.to_h)
          if rec.any_with?("message-id" => entry.first)
            Queue >> entry.first
          end
          rec.done
        end
      end
    end

  end # class Processor

  class Config < OpenStruct; end
  class Account < OpenStruct; end

  module UI
    # ConfigParser related UI

    def print_parse
      puts 'Started parsing the config.'.green
    end

    def print_senders amount
      puts "#{amount} senders found.".yellow
    end

    def print_recievers amount
      puts "#{amount} recievers found.".yellow
    end

    # Queue related UI

    def print_fetch_all
      puts "Fetch all mails.".yellow
    end

    def print_queued mail
      puts "Mail #{mail} was successfully queued.".green
    end

    def print_entry_fetched key, value
      puts "#{key} => #{value} fetched!".yellow
    end

    def print_entry_removed key, value
      puts "#{key} => #{value} removed from store!".red
    end

    def print_keys keys
      puts "Currently stored keys: < #{keys.join(', ')} >".yellow
    end 

    # Sender related UI    

    def print_sent mail
      puts "Sending the mail(#{mail}).".green
    end

    # Reciever related UI    

    def print_found mail
      puts "Mail #{mail} found.".green
    end

    def print_not_found mail
      puts "Mail #{mail} not found.".red
    end

  end # module UI

  class ConfigParser

    extend UI

    class << self

      def parse path
        @conf = YAML.load_file(path)
        print_parse
        return build_config
      end

      def build_config
        h = Hash.new
        h[:queue] = @conf['generic']['queue_store']
        h[:logfile] = @conf['generic']['module_log']
        h[:custom_senders] = accounts(:senders, true).compact
        h[:recievers] = accounts(:recievers, false).compact
        print_recievers h[:recievers].length
        h[:senders] = accounts(:senders, false).compact
        print_senders h[:senders].length
        Config.new h
      end

      private

      def accounts kind , only_reciever
        @conf[kind.to_s].map do |k,v|
          if only_reciever and v.has_key?('reciever')
            Account.new({ name: k}.merge v)
          elsif not only_reciever and not v.has_key?('reciever')
            Account.new({ name: k}.merge v)
          end
        end
      end
    end

  end # class ConfigParser  

  class Queue

    extend UI

    OPTIONS ||= [:filename]
    @@connection={ thread_safe: true }

    class RemoveError < StandardError; end

    class << self

      def set_connection opts
        OPTIONS.each do |opt|
          raise MissingParameterError, 
            'Missing Option: :'+opt.to_s unless opts[opt]
        end
        LOGGER.info 'Set global store connection => ' + opts.inspect
        @@connection.merge! opts
      end

      def << sender
        raise InvalidParameterError, 
          "Expected #{Sender.name}." unless sender.is_a? Sender
        LOGGER.info 'Try to add new entry to queue.'
        send_info = sender.send!
        connection_wrapper do |conn|
          conn[send_info.message_id] = send_info.to.join(' ')
        end
        print_queued send_info.message_id
      end

      def active?
        @@connection[:conn].is_a? PStore
      end

      def path
        @@connection[:filename]
      end

      def empty?
        all.empty?
      end

      def remove_all
        remove_handler
      end

      def remove_all_with key
        remove_handler key
      end

      def keys
        connection_wrapper(true) do |conn|
          print_keys conn.roots
          conn.roots
        end
      end

      def all
        print_fetch_all
        connection_wrapper(true) do |conn|
          conn.roots.map do |entry|
            print_entry_fetched entry, conn[entry] 
            [entry, conn[entry]]
          end
        end
      end

      alias :>> :remove_all_with

      private

      def connection_wrapper ro=false
        @@connection[:conn] = nil unless @@connection[:conn].is_a? PStore
        @@connection[:conn] ||= PStore.new(@@connection[:filename])
        @@connection[:conn].transaction(ro) do
          yield @@connection[:conn]
        end
      end

      def remove_handler(key=nil) 
        connection_wrapper do |conn|
          unless key
            conn.roots.map do |entry|
              print_entry_removed entry, conn[entry]
              conn.delete(entry)
            end
          else
            raise RemoveError, 
              'Entry not found!' unless conn.roots.include? key
            print_entry_removed key, conn[key]
            conn.delete(key)
          end
        end
      end

    end # singleton methods
  end # class Queue

  module Helpers

    class ::String

      def red;  colorize(31)  end
      def green; colorize(32) end
      def yellow; colorize(33) end

      private
      def colorize(color_code)
        "\e[#{color_code}m#{self}\e[0m"
      end

    end # class String

    def self.symbolize_deep! h
      h.keys.each do |k|
        ks    = k.respond_to?(:to_sym) ? k.to_sym : k
        h[ks] = h.delete k 
        symbolize_deep! h[ks] if h[ks].kind_of? Hash
      end
    end

  end # module Helpers

  class Sender

    include UI

    def initialize smtp, mail_info
      LOGGER.debug "SMTP options => #{smtp}"
      LOGGER.debug "MAIL_HEADER => #{mail_info}"

      [smtp, mail_info].each do |h|
        Helpers.symbolize_deep! h
      end

      [:address, :domain, :port, :user_name, :password].each do |k|
        raise MissingParameterError , 
          'Missing SMTP PARAMETER: :'+k.to_s unless smtp[k]
      end

      [:from, :to].each do |k|
        raise MissingParameterError, 
          'Missing MAIL-HEADER: :'+k.to_s unless mail_info[k]
      end

      @smtp_options = { authentication: 'plain', 
                        enable_starttls_auto: true, 
                        openssl_verify_mode: 'none' }
      @smtp_options.merge! smtp
      @mail = mail_info
    end

    def send
      print_sent @mail
      get_prepared_email.deliver
    rescue Net::SMTPFatalError
      OpenStruct.new
    end

    def send!
      print_sent @mail
      get_prepared_email.deliver!
    rescue Net::SMTPFatalError
      OpenStruct.new
    end

    private 

    def get_prepared_email
      _from, _to = [@mail[:from], @mail[:to]]
      LOGGER.debug 'Set delivery data. => '+@mail.inspect
      mail = Mail.new do
        from     _from
        to       _to
        subject  'Mailserver Notify'
        body     'Notified successfull!'
      end
      LOGGER.debug 'Set delivery method. => '+@smtp_options.inspect
      mail.delivery_method :smtp, @smtp_options
      LOGGER.debug 'Mail Object: '+mail.inspect
      mail
    end

  end # class Sender

  require 'net/imap'

  class Reciever

    include UI

    class UnsupportedMethodError < StandardError; end

    SUPPORTED_METHODS ||= ["imap"]
    OPTIONS ||= [:from, :to, :subject, :body, "message-id"]
    attr_reader :mails

    def initialize opts

      Helpers.symbolize_deep! opts
      [:method, :address, :user_name, :password].each do |k|
        raise MissingParameterError, 
          'Missing Authentification information: :'+k.to_s unless opts[k]
      end

      unless SUPPORTED_METHODS.include? opts[:method]
        raise UnsupportedMethodError 
      end

      auth = { enable_ssl: true , folder: 'INBOX'}
      auth.merge! opts

      @mails = Net::IMAP.new auth[:address], 
        ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE }
      @mails.authenticate 'PLAIN' , auth[:user_name], auth[:password]
      @mails.select(auth[:folder])
    end 

    def any_with? param
      arg, value = input_validation param
      if (arg.any?{|x| x =~ /#{value}/})
        print_found value
        true
      else
        print_not_found value
        false
      end
    end

    def done
      @mails.disconnect if @mails.is_a? Net::IMAP
    end

    private

    def input_validation param
      if not param.is_a? Hash
        raise InvalidParameterFormatError, 
          'Parameter should be a hash. Abouting.'       
      elsif not param.keys.length == 1
        raise InvalidParameterFormatError, 
          'Please use only one key at parameter.'
      elsif not OPTIONS.include? param.keys.first
        raise InvalidParameterError, 'Invalid key used.'
      end
      m = @mails.fetch(1..-1, "BODY[HEADER.FIELDS (#{param.keys.first})]")
      [m.map(&:attr).map{|x| x.values.join}, param.values.first]
    end

    def mail_expression_matched? m , value
      case m
      when Array then m.join == value
      when String then m == value
      end
    end

  end # class Reciever

end # module MailserverChecker
