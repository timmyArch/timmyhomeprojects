#!/bin/bash

####
# this should return the ip from an remote host

OPTARG=$2

case $1 in
		
		-v|--vServer|--vserver)
			IP=$(dig node$OPTARG.vpool.your-server.de +short)
			echo $IP
		;;
		-h|--help)
         echo -e "Plese run $0 with one of the following Options
						
							-v NodeID.PoolID
							-r BackupID
							-vMtr  ### same as -v
							-rmtr  ### same as -r

						"
      ;;
		--vMtr)
			IP=$(dig node$OPTARG.vpool.your-server.de +short)
			echo "Do MTR at this Node: $IP , node$OPTARG.vpool.your-server.de"
			mtr $IP
		;;
		-r|--backupserver|--rb)
         IP=$(dig rootbackup$OPTARG.your-server.de +short)
         echo $IP
      ;;
		--rMtr)
         IP=$(dig rootbackup$OPTARG.your-server.de +short)
			COUNT=0
			DOTS=(".")
			curl -s http://note.1n3t.de/cgi-bin/mtr.sh?$IP
			echo "---------------------------------------------------------------------------"
         while true; do 
				VAR=$(curl -s http://note.1n3t.de/cgi-bin/mtr.sh?$IP | tail -n1)
				DOTS+="."
				echo -en "\r$VAR $DOTS"
				sleep 1 
			done
      ;;
		*)
			$0 -h
		;;
esac
